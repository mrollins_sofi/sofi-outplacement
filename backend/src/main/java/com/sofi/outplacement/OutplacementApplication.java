package com.sofi.outplacement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OutplacementApplication {

    public static void main(String[] args) {
        SpringApplication.run(OutplacementApplication.class, args);
    }
}
