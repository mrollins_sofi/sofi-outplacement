package com.sofi.outplacement.repositories;

import com.sofi.outplacement.domain.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.Optional;

@RepositoryRestResource
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    Optional<Appointment> findByCustomerId(String customerId);
}
