package com.sofi.outplacement.controllers;

import com.sofi.outplacement.domain.Appointment;
import com.sofi.outplacement.domain.requests.AppointmentRequest;
import com.sofi.outplacement.domain.responses.BaseResponse;
import com.sofi.outplacement.services.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/outplacement/appointments")
@Api("Operations for working with the outplacement APIs")
public class AppointmentController {

    private AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @ApiOperation(value = "Retrieve appointments", response = Appointment.class, responseContainer = "List")
    @GetMapping
    public ResponseEntity getAppointments() {
        return responseHelper(appointmentService.getAll());
    }

    @ApiOperation(value = "Create a new appointment")
    @PostMapping
    public ResponseEntity createNew(@RequestBody AppointmentRequest appointment){
        appointmentService.save(appointment);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private ResponseEntity responseHelper(BaseResponse response ){
        return response.hasErrors()?
                        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.getError()):
                        ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
