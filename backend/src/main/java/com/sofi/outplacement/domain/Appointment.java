package com.sofi.outplacement.domain;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Appointment extends BaseEntity {
    private String customerId;

    private Date appointmentDate;
    private int duration;
}
