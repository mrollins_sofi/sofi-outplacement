package com.sofi.outplacement.domain.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class BaseResponse {

    @JsonIgnore
    private String error;

    public boolean hasErrors() {
        return StringUtils.isNotBlank(error);
    }
}
