package com.sofi.outplacement.domain.responses;

import com.sofi.outplacement.domain.Appointment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppointmentListResponse extends BaseResponse {
    private List<Appointment> appointmentList;
}
