package com.sofi.outplacement.domain.requests;

import lombok.Data;
import java.util.Date;

@Data
public class AppointmentRequest {
    private Date date;
    private int duration;
}
