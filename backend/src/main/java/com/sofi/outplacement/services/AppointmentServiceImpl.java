package com.sofi.outplacement.services;

import com.sofi.outplacement.domain.Appointment;
import com.sofi.outplacement.domain.requests.AppointmentRequest;
import com.sofi.outplacement.domain.responses.AppointmentListResponse;
import com.sofi.outplacement.repositories.AppointmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppointmentServiceImpl implements AppointmentService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AppointmentRepository appointmentRepo;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepo) {
        this.appointmentRepo = appointmentRepo;
    }

    @Override public AppointmentListResponse getAll() {
        logger.debug("AppointmentServiceImpl:getAll called");
        AppointmentListResponse response = new AppointmentListResponse();
        response.setAppointmentList(appointmentRepo.findAll());
        return response;
    }

    @Override public void save(AppointmentRequest appointmentRequest) {
        Appointment appointment = new Appointment();
        appointment.setAppointmentDate(appointmentRequest.getDate());
        appointment.setDuration(appointmentRequest.getDuration());
        appointmentRepo.save(appointment);
    }
}
