package com.sofi.outplacement.services;

import com.sofi.outplacement.domain.requests.AppointmentRequest;
import com.sofi.outplacement.domain.responses.AppointmentListResponse;

public interface AppointmentService {
    AppointmentListResponse getAll();

    void save(AppointmentRequest appointment);
}
