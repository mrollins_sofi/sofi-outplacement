# Outplacement 
Angular2 and Spring Boot project for Sofi Outplacement 
### To get running:
Clone the repo and build the project from the parent directory

    ~/sofi-outplacement $ mvn clean install
This builds both the frontend(angular) and backend(springboot) 

##### User interface runs from the frontend folder

    ~/sofi-outplacement $ cd frontend/src/main/frontend
    ~/sofi-outplacment/frontend/src/main/frontend $ npm start
    
Should now be running on [http://localhost:4200]()

##### Backend runs from the backend folder (clever?)

    ~/sofi-outplacement $ cd backend
    ~/sofi-outplacement/backend $ mvn spring-boot:run
    
Should be running on [http://localhost:8080]()

#### Swagger
See swagger docs at [http://localhost:8080/swagger-ui.html]()


## Intellij Setup ##
To get the app running in Intellij

1. Clone this repo
2. From intelliJ ```File -> Open ```
3. Browse to the parent folder and select pom.xml and click Open 
4. Backend - Intellij should find the application class for running the backend automatically. Click the run or debug icon.
5. Frontend - In the intelliJ main window, click Terminal on the bottom Toolbar or you can just use your system terminal
![terminal](./README/intellij-terminal.png)
6. ```$ cd frontend/src/main/frontend```
7. ```$ npm start```

URLs for frontend and backend are the same as above
